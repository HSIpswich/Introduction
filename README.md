<img src="image/HS_Logo_WIDE_GoogleLogo.png">

<p>
Here we believe in the power of innovation and human ingenuity, regardless of who you are or what your background is.
</p><p>
At HS Ipswich Inc., we aim to provide a safe, open and friendly space in which our members and the public can work on their projects, collaborate with like-minded individuals, and just hang out.
</p><p>
We are located at U1/199 Brisbane Street QLD 4305. 
</p>
